# README
-------------------------------

### 1. Preparation before using this repository

1. Install the app by using `npm install`. This should install npm and addtional library.
<br>
2. Check out and understand the project structure. The important file are __index.html,styles.css,script.js__
<br>
3. Run the project by using `npm start`. This project run on `http://localhost:8080/`

### 2. Task

1. Adjust CSS according to the design in Figma  [Link](https://www.figma.com/file/L0K6U5TYCMSTpxePb1KHO8gY/Untitled?node-id=0%3A1
)
<br>
2. Add Modal
    - [ ] Welcome Modal
    - [ ] Create Group Modal
    - [ ] Join Group Modal
<br>
3. Implement Function

    - [ ] Create User (Log in)
    - [ ] Join Group
    - [ ] Create Group
    - [ ] Leave Group
    - [ ] Send message to group
    - [ ] Send Message
    - [ ] Receive Unread Message (When leave group temporary)
    - [ ] Temporary Leave

