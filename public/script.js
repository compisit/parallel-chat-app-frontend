new Vue({
  el: '#app'
})


Vue.component('group-name', {
  template: /*html*/`
    <div class="groupname">
      <div class="circle"> A </div>
      <h2 class="d-inline"> Group Name A</h2>
      <div class="line"></div>
    </div>
  `,
})

Vue.component('chat-person', {
  template: /*html*/`
    <div class="chat-person">
      <div class="circle"> C </div>
      <div class="text-message d-inline-block"> Ho ho ho </div>
    </div>
  `,
})

new Vue({
  el: '#left-side',
  data: { hello: 'Hello World!', showModal: true },
  template: /*html*/`
      <div id="left-side">

          <div class="top-bar" style="padding:20px; font-size:24px; color: white; background-color:#413D3D;">
              <p style="display:inline;">Generic Chat app</p>
              <div class="action-icon" style="display:inline-flex;">
                  <i class="fas white-icon" data-toggle="modal" data-target="#exampleModalCreate"></i>
                  <i class="fas white-icon" data-toggle="modal" data-target="#exampleModalJoin"></i>
                  <i class="fas white-icon"></i>
              </div>
          </div>

          <div>
              <group-name v-for="n in 10"/>
          </div>

            
          <!-- Create Modal -->
          <div class="modal fade" id="exampleModalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Create
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Join Modal -->
          <div class="modal fade" id="exampleModalJoin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Join
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
  
        </div>
    `,
  methods: {
    createGroup: function (event) {
      console.log('Create')
    },
    joinGroup: function (event) {
      console.log('Join')
    },
    exitGroup: function (event) {
      console.log('Exit')
    },
    switchGroup: function (evnet) {
      console.log('Switching group')
    }
  }
})


new Vue({
  el: '#right-side',
  data: { hello: 'Hello World!', message: '' },
  template: /*html*/`
    <div>
        <div class="chat-field">
            <chat-person v-for="n in 10"/>  
        </div>

        <div class="text-input">
            <input class="form-control d-inline" placeholder="Enter text" v-model="message">
            <div class="send-btn" v-on:click="sendText">Send</div>
        </div>
    </div>
    `,
  methods: {
    sendText: function (event) {
      console.log('Send Text')
      socket.emit('pingg', this.message)
      this.message = '';
    }
  }
})
